# Ansible Role: Radicale

[![pipeline status](https://gitlab.com/barlebenx/ansible-role-radicale/badges/master/pipeline.svg)](https://gitlab.com/barlebenx/ansible-role-radicale/-/commits/master)

Manage radicale dav server.

## Requirements

- debian 10

## Role Variables

Available variables are listed on `defaults/main.yml`

## Dependencies

None

## Example playbook

```yaml
- hosts: all
  vars:
    radicale_users:
      - name: bob
        password: bob
      - name: alice
        password: $apr1$mysalt$Au6.TISdpaC94hNzoR4MS/
        encrypted: true
  roles:
    - role: radicale
```
